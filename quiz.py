#!/usr/bin/env python

from dataclasses import dataclass
import itertools as it
import json
from os import PathLike
from random import shuffle
from typing import Generator, List


@dataclass
class Question:
    question: str
    options: List[str]
    answer: int


def gen_question(questions: List[Question]) -> Generator[Question, None, None]:
    """Infinite generator of questions in a random order."""
    shuffle(questions)
    question_stream = iter(questions)
    while True:
        try:
            yield next(question_stream)
        except StopIteration:
            shuffle(questions)
            question_stream = iter(questions)


def do_question(question: Question) -> bool:
    """Asks the user a question, and processes their response."""
    print(question['question'])
    response = None
    options = {chr(ord("A") + i): v for i, v in enumerate(question['options'])}
    for key, answer in options.items():
        print(f'{key}: {answer}')
    while response is None or response.upper() not in options:
        if response is not None:
            print('Invalid option selected. Try again:')
        response = input()
    is_correct = ord(response.upper()) - ord('A') == question['answer']
    print("Correct!" if is_correct else "Incorrect", end='\n\n')
    return is_correct


def quiz(quiz_content: List[Question], num_questions: int = 10) -> None:
    """Runs the interactive quiz.

    Quizzes the user with `num_questions` questions taken from `quiz_content`.
    Provides a score after going through the questions, and prompts the user to
    play again.
    """
    print(f'Welcome to the quiz! You will be asked {num_questions} questions')
    if num_questions == 0:
        return
    questions = gen_question(quiz_content)
    while True:
        print('\n')
        num_correct = 0
        for i, question in enumerate(it.islice(questions, num_questions)):
            print(f'Question {i + 1}:')
            num_correct += do_question(question)
        print(f'You got {num_correct/num_questions*100:.2f}%')
        response = input('Would you like to play again? [y/N]: ')
        if response.lower() != 'y':
            break


def load_quiz(quiz_content_file_path: PathLike) -> List[Question]:
    """Loads the quiz content JSON into a list of question objects."""
    with open(quiz_content_file_path) as quiz_content_file:
        return [Question(**x) for x in json.load(quiz_content_file)]


if __name__ == '__main__':
    quiz_content = load_quiz('questions.json')
    try:
        quiz(quiz_content)
    except (EOFError, KeyboardInterrupt):
        pass
    print('\nGoodbye!')
